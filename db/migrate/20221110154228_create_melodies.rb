class CreateMelodies < ActiveRecord::Migration[7.0]
  def change
    create_table :melodies do |t|
      t.string :song
      t.string :artist
      t.string :album
      t.references :record, null: false, foreign_key: true

      t.timestamps
    end
  end
end
